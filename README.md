# Getting Started

### Reference Documentation
### Learning from Linkdin Learning Course - Spring: Test-Driven Development with JUnit - Shashonna Smith
### TDD - Test Driven Development
- Timely Design , Validation , Negotiation and Feedback quickly
- Good Test Coverage - Executable Documentation as well as trustworthy
- Good validation of Story Acceptance Criteria
- Automated Test - Unit Test help to Verify the Individual Part before Integration
- Reading , Writing Test help you to understand code better
- TDD - Good for troubleshooting
- Help New Team members Onboard and Training Easily
- It should be Team Mandate to Have TDD 
- Whenever Stake is High on Live Prod application - New Features should be Developed in TDD Way 
- Service Layer Should be Test Prioritized Much High 
- Have a Good Test Goal/Test Plan

### Test Plan 
- Test Plan should Include What Kind of Test is Required - Unit Testing , Integration Testing
- New Feature - Should be prime candidate for Unit Testing
- Existing well matured features should be Candidate of the Integration Testing
- During Integration Testing always target TesT Data Source -Not the Real Ones should be effected

### Controller Layer Testing
- MVC Style Controller - Return ViewModel
- Rest Type Controller Send JSON , XML 

### Repository Layer testing
- We need to Do only Integration Test - as Repo Methods are already providing the Framework 
- But  if we write our Own Customised Repository Method - then write Integration testing 
- This Testes are done Just to check entity Mapping ,Transaction etc are Fine 
- We need to plan Test based on Data story type - Grpha , NoSql , Relational 
- DBUnit is a Junit extension which help developers to prepare more complex Test Datas
### Test Suit , Interation Test Suit  , Feature Test Suit 
- We can combine Multiple Tests into a Test Suit 
- We can mix and match Test classes as needed for ourTest Suits 

### Continious Integration Testing 
- Whenever there is a TDD -Test Driven Development then We shoud be having a Integration Test Server which continuously
- Test ,Validate - Smkoe Test Suit we can attach here 
- We run this Test whenever Code checkin happen as well as when Full Code build and deplyment happens
- Integration Test Shoud be Light weight 
