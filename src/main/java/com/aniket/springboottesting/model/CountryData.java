package com.aniket.springboottesting.model;

import lombok.Data;

@Data

public class CountryData {
    private String id;
    private String name;
    private String description;
}
