package com.aniket.springboottesting.model;

import lombok.Data;

import java.util.Date;

@Data
public class Response {

    private String status;
    private Date date;
}
