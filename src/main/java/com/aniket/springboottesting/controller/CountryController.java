package com.aniket.springboottesting.controller;

import com.aniket.springboottesting.model.CountryData;
import com.aniket.springboottesting.model.Response;
import com.aniket.springboottesting.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CountryController {

    @Autowired
    private CountryService countryService;

    @GetMapping("/countries")
    public List<CountryData> getAllCountries(){
        return countryService.getAllCountries();
    }


    @GetMapping("/country/{id}")
    public CountryData getCountryById(@PathVariable String id){
        return countryService.getCountryById(id);
    }

    @PutMapping("/country/{id}")
    public CountryData updateCountryById(@PathVariable String id, @RequestBody CountryData countryData){
        return countryService.updateCountryById(id , countryData);
    }

    @PostMapping ("/country")
    public CountryData createCountry( @RequestBody CountryData countryData){
        return countryService.createCountry(countryData);
    }
}
