package com.aniket.springboottesting.service.impl;

import com.aniket.springboottesting.model.Country;
import com.aniket.springboottesting.model.CountryData;
import com.aniket.springboottesting.repository.CountryRepository;
import com.aniket.springboottesting.service.CountryService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class CountryServiceImpl implements CountryService {

    @Autowired
    private CountryRepository countryRepository;
    @Override
    public List<CountryData> getAllCountries() {
        List<Country> countries = StreamSupport
                .stream( countryRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
        List<CountryData> countryDataList = new ArrayList<>();
        countries.forEach(item->{
            CountryData countryData = new CountryData();
            BeanUtils.copyProperties(item,countryData);
            countryData.setDescription("This is additional Data from Service layer List APi call-"+countryData.getName());
            countryDataList.add(countryData);
        });
        return countryDataList;
    }

    @Override
    public CountryData getCountryById(String id) {
        Optional<Country> optionalCountry = countryRepository.findById(id);
        if(optionalCountry.isPresent()){
            CountryData countryData = new CountryData();
             BeanUtils.copyProperties(optionalCountry.get(),countryData);
            countryData.setDescription("This is additional Data from Service layer ID SPECIFIC APi call-"+countryData.getName());
            return  countryData;
        }else{
            throw new RuntimeException("Data Not Found");
        }
    }

    @Override
    public CountryData updateCountryById(String id, CountryData countryData) {
        Optional<Country> optionalCountry = countryRepository.findById(id);
        if(optionalCountry.isPresent()){
            Country country = optionalCountry.get();
            country.setName(countryData.getName());
            Country savedCountry = countryRepository.save(country);
            BeanUtils.copyProperties(savedCountry,countryData);
            countryData.setDescription("This is additional Data from Service layer ID SPECIFIC UPDATE APi call-"+countryData.getName());
            return  countryData;
        }else{
            throw new RuntimeException("Data Not Found fOR update");
        }
    }

    @Override
    public CountryData createCountry(CountryData countryData) {
        Country country = new Country();
        country.setName(countryData.getName());
        Country savedCountry = countryRepository.save(country);
        CountryData countryDataObj = new CountryData();
        BeanUtils.copyProperties(savedCountry,countryDataObj);
        countryDataObj.setDescription("This is additional Data from Service layer FOR SAVE CALL-"+countryDataObj.getName());
        return  countryDataObj;
    }
}
