package com.aniket.springboottesting.service;

import com.aniket.springboottesting.model.CountryData;

import java.util.List;

public interface CountryService {
    List<CountryData> getAllCountries();

    CountryData getCountryById(String id);

    CountryData updateCountryById(String id, CountryData countryData);

    CountryData createCountry(CountryData countryData);
}
