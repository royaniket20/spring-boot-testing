package com.aniket.springboottesting.repository;

import com.aniket.springboottesting.model.Country;
import org.springframework.data.repository.CrudRepository;

public interface CountryRepository extends CrudRepository<Country,String> {
}
