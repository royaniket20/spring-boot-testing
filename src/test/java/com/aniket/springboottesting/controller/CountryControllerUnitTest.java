package com.aniket.springboottesting.controller;

import com.aniket.springboottesting.model.CountryData;
import com.aniket.springboottesting.service.CountryService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CountryController.class) //Unit testing Controller
public class CountryControllerUnitTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CountryService countryService;

    @Before
    public  void init(){
        MockitoAnnotations.openMocks(this);
    }
    @Test
    public  void addCountryHappyPath() throws Exception {
        CountryData countryData = new CountryData();
        countryData.setName("Test Country Controller");
        countryData.setId("Test ID");
        countryData.setDescription("Test Country Description");
        when(countryService.createCountry(any(CountryData.class))).thenReturn(countryData);
        //Mimic Rest api call
        MvcResult mvcResult = mockMvc.perform(post("/country")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{}")).andExpect(status().isOk()).andReturn();
        ObjectMapper objectMapper = new ObjectMapper();
        CountryData result = objectMapper.readValue(mvcResult.getResponse().getContentAsString(), CountryData.class);
        Assert.assertNotNull(result);
        Assert.assertNotNull(result.getId());
        Assert.assertEquals("Test Country Controller",result.getName());
        Assert.assertEquals("Test Country Description",result.getDescription());
    }

}
