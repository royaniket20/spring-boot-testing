package com.aniket.springboottesting.controller;

import com.aniket.springboottesting.model.CountryData;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class CountryControllerIntegrationTest {

    @Autowired
    private CountryController countryController;


    @Test
    public  void addCountryHappyPath(){
        CountryData countryData = new CountryData();
        countryData.setName("Test Country Controller");
        CountryData countrySaved = countryController.createCountry(countryData);
        Assert.assertNotNull(countrySaved);
        Assert.assertNotNull(countrySaved.getId());
        Assert.assertEquals("Test Country Controller",countrySaved.getName());
        Assert.assertEquals("This is additional Data from Service layer FOR SAVE CALL-Test Country Controller",countrySaved.getDescription());
    }
}
