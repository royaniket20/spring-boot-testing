package com.aniket.springboottesting.repository;

import com.aniket.springboottesting.model.Country;
import com.aniket.springboottesting.model.CountryData;
import com.aniket.springboottesting.service.CountryService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest // -- It do whole lots of things -     It is shorthand for Many Internal Annotations
//Like below
//@AutoConfigureDataJPA --Import JPA related Stuffs for testing
//@AutoConfigureTestDatabase -- What kind of Database you want to use for testing
//@AutoConfigureTestEntityManager -- Give access to Entity Manager
//@Transactional --- For Transactional Capabilities - For rollback test data
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE) //Which Database to Use
public class CountryRepositoryIntegrationTest {

    //This can be used to Load some dummy data for Get Data Update data testings
    //End of the Test the data will be cleaned automatically
    @Autowired
    private TestEntityManager testEntityManager;

    @Autowired
    private CountryRepository countryRepository;

    @Test
    public  void addCountryHappyPath(){
        Country countryData = new Country();
        countryData.setName("Test Country");
        Country countrySaved = testEntityManager.persist(countryData);
        Assert.assertNotNull(countrySaved);
        Assert.assertNotNull(countrySaved.getId());
        Assert.assertEquals("Test Country",countrySaved.getName());
    }
}
