package com.aniket.springboottesting.repository;

import com.aniket.springboottesting.model.Country;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace= AutoConfigureTestDatabase.Replace.NONE)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:sampleData.xml")
public class CountryRepositoryDBUnitTest {

    //This can be used to Load some dummy data for Get Data Update data testings
    //End of the Test the data will be cleaned automatically
    // Remember - When you are using the  DB Unit Test Suit - You cannot have the data.sql or schema.sql it will Not allow you to Load data
    @Autowired
    private CountryRepository countryRepository;

    @Test
    public  void addCountryHappyPath(){
        countryRepository.findAll().forEach(item->{
            System.out.println(item.getId());
        });
        Optional<Country> countrySaved = countryRepository.findById("111");
        Assert.assertNotNull(countrySaved.get());
        Assert.assertNotNull(countrySaved.get().getId());
        Assert.assertEquals("Country 111",countrySaved.get().getName());
    }
}
