package com.aniket.springboottesting.service.impl;

import com.aniket.springboottesting.model.CountryData;
import com.aniket.springboottesting.service.CountryService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE) //We will Only do Service LAYER tEST
public class CountryServiceIntegrationTest {
    @Autowired
    private CountryService countryService;

    @Test
    public  void addCountryHappyPath(){
        CountryData countryData = new CountryData();
        countryData.setName("Test Country");
        CountryData countrySaved = countryService.createCountry(countryData);
        Assert.assertNotNull(countrySaved);
        Assert.assertNotNull(countrySaved.getId());
        Assert.assertEquals("Test Country",countrySaved.getName());
        Assert.assertEquals("This is additional Data from Service layer FOR SAVE CALL-Test Country",countrySaved.getDescription());
    }
}
