package com.aniket.springboottesting.service.impl;

import com.aniket.springboottesting.model.Country;
import com.aniket.springboottesting.model.CountryData;
import com.aniket.springboottesting.repository.CountryRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.NONE) //We will Only do Service LAYER Test
public class CountryServiceUnitTest {
    @InjectMocks
    private CountryServiceImpl countryService;

    @Mock
    private CountryRepository countryRepository;

    @Before
    public  void init(){
        MockitoAnnotations.openMocks(this);
    }
    @Test
    public  void addCountryHappyPath(){
        //Expected Mock Data
        Country country = new Country();
        country.setId("DUMMY-ID");
        country.setName("DUMMY-COUNTRY");
        when(countryRepository.save(any(Country.class))).thenReturn(country);

        CountryData countrySaved = countryService.createCountry(new CountryData());
        Assert.assertNotNull(countrySaved);
        Assert.assertNotNull(countrySaved.getId());
        Assert.assertEquals("DUMMY-COUNTRY",countrySaved.getName());
        Assert.assertEquals("This is additional Data from Service layer FOR SAVE CALL-DUMMY-COUNTRY",countrySaved.getDescription());
    }
}
