package com.aniket.springboottesting.testsuits;

import com.aniket.springboottesting.controller.CountryControllerIntegrationTest;
import com.aniket.springboottesting.repository.CountryRepositoryDBUnitTest;
import com.aniket.springboottesting.repository.CountryRepositoryIntegrationTest;
import com.aniket.springboottesting.service.impl.CountryServiceIntegrationTest;
import com.aniket.springboottesting.service.impl.CountryServiceUnitTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) //We want to Create Test Suit
@Suite.SuiteClasses({CountryRepositoryDBUnitTest.class , CountryRepositoryIntegrationTest.class, CountryServiceIntegrationTest.class , CountryServiceUnitTest.class})
public class CountryServiceRepoTestSuit {
    //No Implementation Needed
}
