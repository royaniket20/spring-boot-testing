package com.aniket.springboottesting.testsuits;

import com.aniket.springboottesting.controller.CountryControllerIntegrationTest;
import com.aniket.springboottesting.repository.CountryRepositoryDBUnitTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class) //We want to Create Test Suit
@Suite.SuiteClasses({CountryControllerIntegrationTest.class , CountryRepositoryDBUnitTest.class})
public class CountryControllerTestSuit {
    //No Implementation Needed
}
